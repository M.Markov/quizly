import React, { useState, useEffect } from 'react';
import Loading from '../../components/Loading/Loading';
import httpProvider from '../../providers/httpProvider';
import { BASE_URL } from '../../common/constants';
import { useHistory } from "react-router-dom";
import PropTypes from 'prop-types';
import CategoryQuizzes from '../../components/CategoryQuizzes/CategoryQuizzes';
import QuizHistory from '../../components/QuizHistory/QuizHistory/QuizHistory';
import SolveQuiz from '../../components/SolveQuiz/SolveQuiz/SolveQuiz';
import AlertDialog from '../../components/AlertDialog/AlertDialog';
import AppError from '../../components/AppError/AppError';


const Quizzes = (props) => {

    const [categoryData, setCategoryData] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [alert, setAlert] = useState({ open: false, msg: null });
    const [quizContent, setQuizContent] = useState(null);
    const [attempt, setAttempt] = useState(null);
    const [quizHistory, setQuizHistory] = useState(null);
    const history = useHistory();
    const { categoryId, quizId } = props.match.params;

    const solveQuizPage = props.location.pathname === `/categoties/${categoryId}/quizzes/${quizId}`;
    const quizHistoryPage = props.location.pathname === `/categoties/${categoryId}/quizzes/${quizId}/history`;
    useEffect(() => {
        setLoading(true)
        httpProvider.getAll(`${BASE_URL}/categories/${categoryId}`, `${BASE_URL}/categories/${categoryId}/quizzes`)
            .then(result => {
                const fails = result.find(res => res.message);
                if(fails) {
                    throw new Error(fails.message);
                }
                setCategoryData(result);
            })
            .catch(error => setError(error.message))
            .finally(() => setLoading(false))
    }, [attempt]);

    const getQuizContent = () => {
        setLoading(true)
        httpProvider.get(`${BASE_URL}/categories/${categoryId}/quizzes/${quizId}`)
            .then(result => {
                if (result.message) {
                    throw new Error(result.message);
                  }
                setQuizContent(result);
            })
            .catch(error => setError(error.message))
            .finally(() => {
                setLoading(false);
            })
    };

    const submitQuizAttempt = (data) => {
        setLoading(true)
        httpProvider.put(`${BASE_URL}/categories/${categoryId}/quizzes/${quizId}`, data)
            .then(result => {
                if (result.message) {
                    throw new Error(result.message);
                  }
                setAttempt(result);
                history.replace(`/categoties/${categoryId}/quizzes`);
                setAlert({ open: true, msg: `Congraduations! You made ${result.attempt} points out of ${result.totalPoints}!` });
            })
            .catch(error => setError(error.message))
            .finally(() => {
                setLoading(false);
            })
    };

    const getQuizHistory = (searchParams) => {
        httpProvider.get(`${BASE_URL}/categories/${categoryId}/quizzes/${quizId}/history${searchParams}`)
        .then(result => {
            if (result.message) {
                throw new Error(result.message);
              }
             setQuizHistory(result);
        })
        .catch(error => setError(error.message))

    }

    if(error) {
        return <AppError message={error} />;
    };
    if (loading) {
        return <Loading />
    };


    if (quizHistoryPage  && quizHistory && quizHistory?.records?.[0]?.id === +quizId) {
        return <QuizHistory quizHistoryData={quizHistory} searchHandler={getQuizHistory} />;
    };

    if (quizHistoryPage && categoryData) {
        const searchParams =  window.location.search;
        getQuizHistory(searchParams);
    };


    if (solveQuizPage && quizContent) {
        return <SolveQuiz quizContent={quizContent} submitQuizAttempt={submitQuizAttempt} />;
    }
    if (solveQuizPage && categoryData) {
        if (categoryData[1].quizzes.some(quiz => quiz.id === quizId && isSolved)) {
            history.push(`/categoties/${categoryId}/quizzes?solving=denied`);
            return setAlert({ open: true, msg: "You have already solve the searching quiz" });
        }
        return getQuizContent();
    };

    
    return (
      <>
        <AlertDialog alert={alert} close={() => setAlert({ open: false, msg: null })} />
        {categoryData ?
          <CategoryQuizzes quizzes={categoryData[1].quizzes} category={categoryData[0]}>
            {/* <Pagination currentPage={page} maxPages={maxPages} change={setQuerryParams} /> */}
          </CategoryQuizzes> : null}
      </>
        )
    };

Quizzes.propTypes = {
    location: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired
}
export default Quizzes;
