import React, { useContext } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container'; 
import { AuthContext, extractUser } from '../../context/AuthContext';
import { BASE_URL } from '../../common/constants';
import { Formik } from "formik";
import * as Yup from 'yup';
import httpProvider from '../../providers/httpProvider';
import storageProvider from '../../providers/storageProvider';
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const SignIn = () => {
  const { setLogin } = useContext(AuthContext);
  const history = useHistory();
  const classes = useStyles();
  const initialValues = {
    username: "",
    password: ""
  };

  const submitForm = (values) => {
    httpProvider.post(`${BASE_URL}/session`, values)
    .then(result => {
      storageProvider.setToken(result.token);
      setLogin({
        isLoggedIn: !!extractUser(result.token),
        user: extractUser(result.token),
      });
    }).finally(() => history.push('/dashboard'))
  };

  const signInSchema = Yup.object().shape({
    username: Yup.string('Username must be text.')
    .min(3, 'Username must be not less than 2 characters.')
    .max(25, 'Username must be not more than 25 characters.')
    .required("Required"),
    password: Yup.string('Password must be valid combination of letters and symbols')
    .min(3, 'Password must be not less than 2 characters.')
    .max(25, 'Password must be not more than 25 characters.')
    .required("Required"),
  })

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={signInSchema}
      onSubmit={submitForm}
    >
      {(formik) => {
        const {
          values,
          handleChange,
          handleSubmit,
          errors,
          touched,
          handleBlur,
          isSubmitting,
        } = formik;
        return (
          <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
              <Avatar className={classes.avatar} style={{'backgroundColor': '#7CFC00', 'color': 'black'}}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Sign in
              </Typography>
              <form className={classes.form} noValidate>
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id="username"
                  label="Username"
                  name="username"
                  autoComplete="username"
                  autoFocus
                  value={values.username}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  helperText={touched.username ? errors.username : ""}
                  error={touched.username && Boolean(errors.username)}
                />
                <TextField
                  variant="outlined"
                  margin="normal"
                  value={values.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  helperText={touched.password ? errors.password : ""}
                  error={touched.password && Boolean(errors.password)}
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                />
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                  disabled={isSubmitting}
                  onClick={handleSubmit}
                >
                  Sign In
                </Button>
                <Grid container>
                  <Grid item>
                    <Link href="/register" variant="body2">
                      <p>Don't have an account? Sign Up</p>
                    </Link>
                  </Grid>
                </Grid>
              </form>
            </div>
          </Container>
        )
      }}
    </Formik>
  )
};

export default SignIn;
