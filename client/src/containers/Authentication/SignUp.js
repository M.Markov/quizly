import React, { useState, useContext } from 'react';
import {Avatar, Button, TextField, Link, Grid, Typography, Container} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { AuthContext, extractUser } from '../../context/AuthContext';
import { BASE_URL } from '../../common/constants';
import storageProvider from '../../providers/storageProvider';
import { Formik } from "formik";
import { useHistory } from "react-router-dom";
import * as Yup from 'yup';
import httpProvider from '../../providers/httpProvider';
import AppError from '../../components/AppError/AppError';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const SignUpForm = () => {
  const { setLogin } = useContext(AuthContext);
  const history = useHistory();
  const classes = useStyles();
  const [error, setError] = useState(null);

  const formFields = { 
    username: {
      name: 'Username',
      type: 'text',
      placeholder: 'username',
      value: '',
      touched: false
    },
    password: {
      name: 'Password',
      type: 'password',
      placeholder: 'password',
      value: '',
      touched: false
    },
    confirmPassword: {
      name: 'Confirm Password',
      type: 'password',
      placeholder: 'confirmPassword',
      value: '',
      touched: false
    },
    firstname: {
      name: 'First Name',
      type: 'text',
      placeholder: 'firstname',
      value: '',
      touched: false
    },
    lastname: {
      name: 'Last Name',
      type: 'text',
      placeholder: 'lastname',
      value: '',
      touched: false
    },
  };

  const initialValues = Object.entries(formFields).reduce((initialValues, [k, v]) => {
    return { ...initialValues,[k]: v.value}
  }, {})

  const signUpSchema = Yup.object().shape({
    username: Yup.string('Username must be text.')
    .min(3, 'Username must be not less than 2 characters.')
    .max(25, 'Username must be not more than 25 characters.')
    .required("Required"),
    password: Yup.string('Password must be valid combination of letters and symbols')
    .min(3, 'Password must be not less than 2 characters.')
    .max(25, 'Password must be not more than 25 characters.')
    .required("Required"),
    confirmPassword: Yup.string().oneOf([Yup.ref('password'), null], 'Passwords must match').required('Required'),
    firstname: Yup.string('Username must be text.')
    .min(3, 'Username must be not less than 2 characters.')
    .max(25, 'Username must be not more than 25 characters.')
    .required("Required"),
    lastname: Yup.string('Username must be text.')
    .min(3, 'Username must be not less than 2 characters.')
    .max(25, 'Username must be not more than 25 characters.')
    .required("Required"),
  })

  const handleRegister = (userInfo) => {
    httpProvider.post(`${BASE_URL}/session/signup`, userInfo)
    .then(res => {
      if (res.message) {
        throw new Error(res.message);
      }
      storageProvider.setToken(res.token);
      setLogin({
        isLoggedIn: !!extractUser(res.token),
        user: extractUser(res.token),
      });
    })
    .catch(error => setError(error.message))
    .finally(() => history.push('/dashboard'))
  }

  if(error) {
    return <AppError message={error} />;
  }

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={signUpSchema}
      onSubmit={handleRegister}
    >
      {(formik) => {
        const {
          values,
          handleChange,
          handleSubmit,
          errors,
          touched,
          isValid,
          handleBlur,
        } = formik;
        return (
          <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
              <Avatar className={classes.avatar} style={{'backgroundColor': '#7CFC00', 'color': 'black'}}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Sign up
              </Typography>
              <form className={classes.form} noValidate>
                <Grid container spacing={2}>
                  {Object.keys(formFields)
                    .map((name) => ({ name, config: formFields[name] }))
                    .map(({ name, config }) => {
                      return (
                        <Grid item xs={12} key={name}>
                          <TextField
                            autoComplete={config.name}
                            name={name}
                            variant="outlined"
                            required
                            fullWidth
                            type={config.type}
                            id={name}
                            label={config.name}
                            value={values[name]}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            helperText={touched[name] ? errors[name] : ''}
                            error={touched[name] && Boolean(errors[name])}
                          />
                        </Grid>
                  );
                  })}
                </Grid> 
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                  onClick={handleSubmit}
                  disabled={!isValid}
                >
                  Sign Up
                </Button>
                <Grid container justify="flex-end">
                  <Grid item>
                    <Link href="/login" variant="body2">
                      Already have an account? Sign in
                    </Link>
                  </Grid>
                </Grid>
              </form>
            </div>
          </Container>
        )
      }}

    </Formik>
  )
}

export default SignUpForm;
