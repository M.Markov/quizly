/* eslint-disable react/jsx-boolean-value */
import React, { useState, useEffect, useContext } from 'react';
import { BASE_URL } from '../../common/constants';
import AuthContext from '../../context/AuthContext';
import httpProvider from '../../providers/httpProvider';
import Loading from '../../components/Loading/Loading';;
import UserHistory from '../../components/UserHistory/UserHistory';
import CreateCategory from '../../components/CreateCategory/CreateCategory';
import { Button } from '@material-ui/core';
import AlertDialog from '../../components/AlertDialog/AlertDialog';
import CreateQuiz from '../../components/CreateQuiz/CreateQuiz/CreateQuiz';
import {useHistory} from 'react-router-dom';
import Categories from '../../components/Categories/Categories';
import {Container, Row, Col} from 'react-bootstrap';
import './TeachersDashboard.css';
import AddIcon from '@material-ui/icons/Add';
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
import AppError from '../../components/AppError/AppError';

const TeachersDashboard = () => {

  const [dashboardData, setDashboardData] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [alert, setAlert] = useState({open:false, msg: null});
  const [newEntry, setNewEntry] = useState(false);
  const history = useHistory();
  const { user } = useContext(AuthContext);
  const hasCategories = dashboardData ? dashboardData[0].count > 0 :  null;

  useEffect(() => {
    setLoading(true);
    httpProvider
      .getAll(`${BASE_URL}/categories`, `${BASE_URL}/users/${user.sub}/history`)
      .then(res => {
        if (res.message) {
          throw new Error(res.message);
        }
        setDashboardData(res);
      })
      .catch(error => setError(error.message))
      .finally(() => setLoading(false));
  }, [newEntry]);


  if (loading) {
    return <Loading />
  }

  if(error) {
    return <AppError message={error} />;
  }

  const createCategoryHandler = (values) => {
    if (dashboardData[0]?.categories.some(category => category.name === values.name)) {
     return setAlert({open: true, msg: "There is already category with the same name"});
    };
    httpProvider
      .postFormData(`${BASE_URL}/categories`, values)
      .then(res => {
        const newDashboardData = JSON.parse(JSON.stringify(dashboardData));
        newDashboardData[0].categories.push(res);
        newDashboardData[0].count++;
        setDashboardData(newDashboardData);
        setAlert({open: true, msg: `Category ${res.name} successfuly added to the collection!`});
      })
  };

  const createQuizHandler = (values) => {
    setLoading(true);
    values.duration = values.duration * 60;
    httpProvider
      .post(`${BASE_URL}/categories/${values.category}/quizzes`, values)
      .then(res => {
        if (res.message) {
          throw new Error(res.message);
        }
        history.push('/dashboard');
      })
      .catch(error => setError(error.message))
      .finally(() => {
        setAlert({open: true, msg: `The new quiz ${values.name} was succesfully added to the colection ! `});
        setNewEntry(true);
        setLoading(false);
      });
  };

  if(dashboardData && location.pathname === '/quizzes/create') {
    if(hasCategories) {
      return <CreateQuiz create={createQuizHandler} categories={dashboardData[0].categories} />
    };
      history.push('/dashboard?create=denied');
  };


  const createQuizButtonHandler = () => {
    if(hasCategories) {
      setNewEntry(false);
      return history.push('/quizzes/create');
    }
    history.push('/dashboard?create=denied');
    setAlert({open: true, msg: "There are no categories yet. Please first create one."});
  };

  return (
    <>
      <AlertDialog alert={alert} close={() => setAlert({open:false, msg: null})} />
      <Container className="teacher-container">
        <Row>
          <Col md="auto">
            <Button variant="contained" size="large" fullWidth color="primary" startIcon={<LibraryBooksIcon />} onClick={createQuizButtonHandler}>Create Quiz</Button>
            <CreateCategory create={createCategoryHandler} />
          </Col>
          <Col md="auto">
            {dashboardData ? <UserHistory data={dashboardData[1]} /> : null}
          </Col>
          <Col lg={true}>
            <h3 className="categories-title">CURRENT CATEGORIES LIST</h3>
            <Row className="categories">
              {dashboardData ? <Categories categories={dashboardData[0].categories} /> : null}
            </Row>
          </Col>
        </Row>
      </Container>
    </>
  )
};

export default TeachersDashboard;
