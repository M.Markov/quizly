import './App.css';
import React, {useState} from "react";
import {BrowserRouter as Router, Redirect, Route, Switch} from 'react-router-dom';
import {AuthContext, extractUser} from './context/AuthContext';
import GuardedRoute from './context/GuardedRoute';
import StudentsDashboard from './containers/StudentsDashBoard/StudentsDashboard';
import TeachersDashboard from './containers/TeachersDashBoard/TeachersDashboard';
import NavBar from './components/NavBar/NavBar';
import SignIn from './containers/Authentication/SignIn';
import SignUp from './containers/Authentication/SignUp';
import storageProvider from './providers/storageProvider';
import Quizzes from './containers/Quizzes/Quizzes';
import { roles } from './common/constants';
import NotFound from './components/NotFound/NotFound';

function App() {

  const [auth, setAuth] = useState({
    isLoggedIn: !!extractUser(storageProvider.getToken()),
    user: extractUser(storageProvider.getToken())
  });

  return (
    <Router>
      <AuthContext.Provider value={{...auth, setLogin: setAuth}}>
        <NavBar />
        <Switch>
          <Redirect path="/" exact to="/login" />
          <Route path="/login" exact component={SignIn} />
          <Route path="/register" exact component={SignUp} />
          <GuardedRoute path="/dashboard" auth={auth.isLoggedIn} exact component={auth.user?.role === roles.DEFAULT_USER_ROLE ? StudentsDashboard : TeachersDashboard} />
          <GuardedRoute path="/users/leaderboard" auth={auth.isLoggedIn && auth.user.role === roles.DEFAULT_USER_ROLE} exact component={StudentsDashboard} />
          <GuardedRoute path="/quizzes/history" auth={auth.isLoggedIn} exact component={StudentsDashboard} />
          <GuardedRoute path="/quizzes/create" auth={auth.isLoggedIn && auth.user.role ===  roles.ADVANCED_USER_ROLE} exact component={TeachersDashboard} />
          <GuardedRoute path="/categoties/:categoryId/quizzes" auth={auth.isLoggedIn} exact component={Quizzes} />
          <GuardedRoute path="/categoties/:categoryId/quizzes/:quizId" auth={auth.isLoggedIn} exact component={Quizzes} />
          <GuardedRoute path="/categoties/:categoryId/quizzes/:quizId/history" auth={auth.isLoggedIn && auth.user.role ===  roles.ADVANCED_USER_ROLE} exact component={Quizzes} />
          <Route path="*" component={NotFound} />
        </Switch>
      </AuthContext.Provider>
    </Router>
  );
}

export default App;
