import React from 'react';
import TextField from '@material-ui/core/TextField';
import {  Grid, FormControl, Select, InputLabel, MenuItem, CardContent, CardHeader, Card, FormHelperText } from '@material-ui/core';
import PropTypes from 'prop-types';


const QuizDetails = ({classes, input, categories, handleChangeInput}) => {


    return(
      <Card className={classes.myCard}>
        <CardHeader title="Basic information" />
        <CardContent>
          <Grid item xs={12} key='question-name'>
            <TextField
              helperText={input.name.errorMsg ? input.name.errorMsg : null}
              error={!input.name.valid && input.name.touched}
              name="name"
              label="Name"
              value={input.name.value}
              onClick={(event) => handleChangeInput({target: {name: 'name', value: event.target.value || ''}}, 'input')}
              onChange={(event) => handleChangeInput(event, 'input')}
              variant="outlined"
              fullWidth
              margin="dense"
            />
          </Grid>
          <Grid item xs={12} key='duration'>
            <FormControl
              helperText={input.category.errorMsg}
              error={!input.category.valid && input.category.touched} variant="outlined" className={classes.categorySelect}
            >
              <InputLabel id="demo-simple-select-outlined-label">Category</InputLabel>
              <Select
                labelId="demo-simple-select-outlined-label"
                name="category"
                value={input.category.value}
                onClick={event => handleChangeInput({target: {name: 'category', value: event.target.value || ''}}, 'input')}
                label="Category"
              >
                {categories.map(category =>  <MenuItem key={category.id} value={category.id}>{category.name}</MenuItem>)}
              </Select>
              <FormHelperText>{input.category.errorMsg}</FormHelperText>
            </FormControl>
            <TextField
              error={!input.duration.valid && input.duration.touched}
              InputProps={{ classes: { input: classes.duration } }}
              name="duration"
              label="Duration in minutes"
              type="number"
              value={input.duration.value}
              onClick={event => handleChangeInput({target: {name: 'duration', value: event.target.value || ''}}, 'input')}
              onChange={event => handleChangeInput(event, 'input')}
              variant="outlined"
              margin="dense"
              helperText={input.duration.errorMsg}
            />
          </Grid>
          <FormHelperText error={!input.questions.valid && input.questions.touched} margin="dense">{input.questions.errorMsg}</FormHelperText>
        </CardContent>
      </Card>
    )
}
export default QuizDetails;

QuizDetails.propTypes = {
    handleChangeInput: PropTypes.func.isRequired,
    categories:PropTypes.array.isRequired,
    input:PropTypes.object.isRequired,
    classes:PropTypes.object.isRequired,
  }
