/* eslint-disable react/prop-types */
import React from "react";
import { withFormik } from "formik";
import * as Yup from 'yup';
import { Fab, Button, TextField,CardActions, CardContent,CardHeader,Card, withStyles} from '@material-ui/core';
import AddIcon from "@material-ui/icons/Add";


const styles = () => ({
  card: {
    maxWidth: 500,
    marginTop: 10
  },
  actions: {
    float: "right"
  }
});

const form = props => {
  const {
    classes,
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
    handleReset,
    setFieldValue
  } = props;

  return (
    <form onSubmit={handleSubmit}>
      <Card className={classes.card}>
        <CardHeader title="CREATE CATEGORY" />
        <CardContent>
          <TextField
            id="name"
            label="Name"
            value={values.name}
            onChange={handleChange}
            onBlur={handleBlur}
            helperText={touched.name ? errors.name : ""}
            error={touched.name && Boolean(errors.name)}
            margin="dense"
            variant="outlined"
            fullWidth
          />
          <TextField
            id="description"
            label="Description"
            value={values.description}
            onChange={handleChange}
            onBlur={handleBlur}
            helperText={touched.description ? errors.description : ""}
            error={touched.description && Boolean(errors.description)}
            margin="dense"
            variant="outlined"
            fullWidth
            multiline
          />
          <label htmlFor="cover">
            <input
              style={{ display: "none" }}
              id="cover"
              type="file"
              accept="image/*"
              onChange={(event) => setFieldValue("cover", event.currentTarget.files[0])}
              onBlur={handleBlur}
            />
            <Fab
              color="primary"
              size="small"
              component="span"
              aria-label="add"
              variant="extended"
              helperText={touched.cover ? errors.cover : ""}
              error={touched.cover && Boolean(errors.cover)}
            >
              <AddIcon /> Upload cover
            </Fab>
          </label>
        </CardContent>
        <CardActions className={classes.actions}>
          <Button type="submit" color="primary" disabled={isSubmitting}>
            CREATE
          </Button>
          <Button color="secondary" onClick={handleReset}>
            CLEAR
          </Button>
        </CardActions>
      </Card>
    </form>
  );
};


  const CreateCategoty = withFormik({
    mapPropsToValues: ({
      name,
      description,
      cover,
    }) => {
      return {
          name: name || "",
          description: description || "",
          cover: cover || "",
      };
    },
  
    validationSchema: Yup.object().shape({
      name: Yup.string('Name must be text.')
      .min(2, 'Name must be not less than 2 characters.')
      .max(25, 'Name must be not more than 25 characters.')
      .required("Required"),
      description: Yup.string()
      .min(3, 'Description must be not less than 3 characters.')
      .max(300, 'Description must be not more than 300 characters.')
      .required("Required"),
      cover: Yup.mixed()
      .required("Required")
      .test("fileFormat", "Unsupported Format", (value) => !(value && !['image/png', 'image/jpeg'].includes(value.type))),
    }),
  
    handleSubmit: (values, bag) => {
        bag.props.create(values);
        bag.resetForm();
    }
  })(form);


export default withStyles(styles)(CreateCategoty);
