import React from 'react';
import PropTypes from 'prop-types';
import CollapsibleTable from '../CollapsibleTable/CollapsibleTable'
import {Box} from '@material-ui/core';
import Pagination from '../Pagination/Pagination';

const Leaderboard = (props) => {
  return(
    <Box pt={2} display='flex' alignItems='center' justifyContent='center'>
      
      <Box md={6} width={500}>
        {props.data ? <CollapsibleTable data={props.data.users} setSearchParams={props.setQuerryParams} /> : null}
        <Pagination data={props.data} change={props.setQuerryParams} />
      </Box>
    </Box>
  )
};

Leaderboard.propTypes = {
  data: PropTypes.object.isRequired,
  setQuerryParams: PropTypes.object.isRequired,
}

export default Leaderboard;
