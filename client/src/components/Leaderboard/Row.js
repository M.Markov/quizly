import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

export default function Row(props) {
  const { row } = props;
  const [open, setOpen] = useState(false);
  const classes = makeStyles({
      root: {
          '& > *': {
              borderBottom: 'unset',
          },
      },
  });
  
  return (
    <>
      <TableRow className={classes.root}>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={() =>  setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row">
          {row.Username ? row.Username : row.category}
        </TableCell>
        <TableCell align="left">{row.Rank ? row.Rank : row.points}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={3}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Typography variant="h6" gutterBottom component="div">
                Info
              </Typography>
              <Table size="small" aria-label="user-info">
                <TableHead>
                  <TableRow>
                    {row ? Object.keys(row).filter(el => ![ 'Rank', 'category', 'id' ].includes(el)).map(el => <TableCell align="left" key={row.id}>{el}</TableCell>) : null}
                  </TableRow>
                </TableHead>
                <TableRow>
                  {row ? Object.entries(row).filter(el => ![ 'Rank', 'category', 'id' ].includes(el[0])).map(el => <TableCell align="left" key={row.id}>{el[1]}</TableCell>) : null}
                </TableRow> 
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  )
};

Row.propTypes = {
  row: PropTypes.object.isRequired
};
