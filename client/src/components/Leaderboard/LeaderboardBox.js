import React from 'react';
import PropTypes from 'prop-types';
import {Box, Table, TableBody, TableCell, TableHead, TableRow, Link } from '@material-ui/core';

const LeaderboardBox = props => {
  const rows = props.data?.users;

  return (
    <Box style={{width: 400}}>
      <h6 style={{marginLeft: 150}}>Top 10 users</h6>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell align="left">Rank</TableCell>
            <TableCell align="left">Username</TableCell>
            <TableCell align="right">Points</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows?.map((row) => (
            <TableRow key={row.id}>
              <TableCell>{row.Rank}</TableCell>
              <TableCell align="left">{row.Username}</TableCell>
              <TableCell align="right">{row.Points}</TableCell>
            </TableRow>
            ))}
        </TableBody>
      </Table>
      <div>
        <Link color="primary" href="/users/leaderboard?page=1">
          See others
        </Link>
      </div>
    </Box>
  )
};

LeaderboardBox.propTypes = {
  data: PropTypes.object.isRequired,
}

export default LeaderboardBox;
