import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import { Menu, MenuItem } from '@material-ui/core/';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import logo from '../../img/favicon.ico';
import { AuthContext } from '../../context/AuthContext';
import storageProvider from '../../providers/storageProvider';
import { BASE_URL } from '../../common/constants';
import httpProvider from '../../providers/httpProvider'
import DashboardIcon from '@material-ui/icons/Dashboard';
import MenuBookIcon from '@material-ui/icons/MenuBook';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';

export default function NavBar() {
  const history = useHistory();
  const { isLoggedIn, user, setLogin } = useContext(AuthContext);
  const [anchorEl, setAnchorEl] = useState(null);
  const [expanded, setExpanded] = useState('');
  const [userInfo, setUserInfo] = useState(null);

  const getUserProfile = async (event) => {
    event.preventDefault();
    httpProvider.get(`${BASE_URL}/users/${user.sub}`)
      .then(data => setUserInfo(data))
  }

  const handleChange = (panel) => (event, newExpanded) => {
    event.preventDefault()
    setExpanded(newExpanded ? panel : false);
  };

  const handleClose = () => {
    setAnchorEl(null);
    setExpanded(false);
  };

  const logout = () => {
    storageProvider.removeToken();
    setLogin({
      isLoggedIn: false,
      user: null,
    });
    setAnchorEl(null);

    return (history.push('/'));
  }

  const dashboardHandler = () => {
    setAnchorEl(null);

    history.push('/dashboard');
  };


  if (isLoggedIn) {
    return (
      <AppBar position="static" style={{ 'backgroundColor': 'black' }}>
        <Toolbar>
          <img src={logo} alt="logo" />
          <p style={{ 'position': 'absolute', 'right': '9%', 'color': '#7CFC00', 'top': '35%' }}>{user.subFullname}</p>
          <Button
            color="inherit"
            // aria-controls="menu"
            // aria-haspopup="true"
            style={{ 'position': 'absolute', 'right': '5%', 'color': '#7CFC00' }}
            onClick={e => {
              // console.log(e);
              setAnchorEl(e.currentTarget);
              getUserProfile(e)
            }}
          ><AccountCircleIcon style={{ 'fontSize': '35px' }} />
          </Button>
          <Menu
            id="menu"
            style={{ 'position': 'absolute', 'top': '5%' }}
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem onClick={dashboardHandler}><DashboardIcon />   Dashboard </MenuItem>
            <Accordion square expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
              <AccordionSummary aria-controls="panel1d-content" id="panel1d-header">
                <MenuBookIcon />
                <Typography style={{ 'paddingLeft': '2px' }}>Profile</Typography>
              </AccordionSummary>
              <AccordionDetails>
                {user.role === 'teacher' ?
                  <Typography>Created Categories: {userInfo?.categoriesCount}</Typography>
                  :
                  <Typography>Rank: {userInfo?.studentsRank}</Typography>}
              </AccordionDetails>
              <AccordionDetails>
                {user.role === 'teacher' ?
                  <Typography>Created Quizzes: {userInfo?.quizzesCount}</Typography>
                  :
                  <Typography>Points Earned: {userInfo?.totalPoints}</Typography>}
              </AccordionDetails>
              {user.role === 'student' ?
                <AccordionDetails>
                  <Typography>Solved Quizzes: {userInfo?.quizzesCount}</Typography>
                </AccordionDetails>
                :
                null}
            </Accordion>
            <MenuItem onClick={logout}><ExitToAppIcon />Logout</MenuItem>
          </Menu>
        </Toolbar>
      </AppBar>
    );
  } 
    return null;
}
