/* eslint-disable react/jsx-handler-names */
import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';

export default function AlertDialog(props) {

  return (
    <div>
      <Dialog
        open={props.alert.open}
        onClose={props.close}
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="customized-dialog-title">
          Quizly
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {props.alert.msg}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={props.close} color="primary">
            OK
          </Button>
          {props.children}
        </DialogActions>
      </Dialog>
    </div>
  );
}

AlertDialog.propTypes = {
    alert: PropTypes.object.isRequired,
    close: PropTypes.func.isRequired,
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node
  ])
  }
