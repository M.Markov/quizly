import React from 'react';

const NotFound = () => {

  return(
    <div id="wrapper" className='text-center'>
      <img src="https://cdn.dribbble.com/users/285475/screenshots/2083086/dribbble_1.gif" />
      <div id="info" style={{position:'absolute', top: '20%',right: 0,left: 0, zIndex: 1}}>
        <h1>404</h1>
        <h3>This page could not be found</h3>
      </div>
    </div>
  )
} 

export default NotFound;
