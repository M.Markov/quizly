import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import SearchIcon from '@material-ui/icons/Search';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));

const SearchName = (props) => {
  const [name, setName] = useState('')
  const classes = useStyles();

  const handleSubmit = (event) => {
    event.preventDefault();
    let currentUrlParams = new URLSearchParams(window.location.search);
    currentUrlParams.set('page', 1);
    props.location.pathname === '/users/leaderboard' ? currentUrlParams.set('username', name) : currentUrlParams.set('name', name);
    props.history.push({ search: '?' + currentUrlParams.toString() });
  }
  
  return (
    <form className={classes.root} noValidate autoComplete="off" onSubmit={ev => handleSubmit(ev)}>
      <TextField type='text' name='name' value={name} onChange={event => setName(event.target.value)} style={{width: 260}} label={props.location.pathname === '/users/leaderboard' ? 'Search username' : 'Search quiz name'} />
      <Button variant="contained" type="submit" color="primary">
        <SearchIcon /> Search
      </Button>
    </form>
  );
}

SearchName.propTypes = {
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
}

export default withRouter(SearchName)
