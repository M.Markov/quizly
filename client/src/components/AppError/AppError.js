import React from 'react';
import PropTypes from 'prop-types';
import './AppError.css';

const AppError = ({ message }) => {

  return (
    <div id="notfound">
      <div className="notfound">
        <div className="notfound-404">
          <h1>Oops!</h1>
          <h2>{message}</h2>
        </div>
        <a href="/dashboard">Go TO Dashboard</a>
      </div>
    </div>
  )
};

AppError.propTypes = {
  message: PropTypes.string.isRequired,
};

export default AppError;
