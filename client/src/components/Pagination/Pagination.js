/* eslint-disable react/prop-types */
import React from 'react';
import './Pagination.css';
import PropTypes from 'prop-types';
function Pagination({ data, change }) {
  let leftSide = Math.max(data.currentPage - 1, 1)
  let rightSide = Math.min(data.currentPage + 1, data.count);

  const changePage = (move) => {
    const {currentPage} = data;
    let newPage = currentPage;

    if (move === 'prev') {
      newPage--;
    }
    if (move === 'next') {
      newPage++;
    }
    if (typeof move === 'number') {
      newPage = move;
    }

    if (newPage === currentPage) {
      return
    }
    return change('page', newPage);
  };

  
  const items = Array.from({ length: (rightSide - leftSide + 1) }, (_, i) => leftSide + i)
    .map(number => {
      return (<div key={number} className={(number === data.currentPage ? 'round-effect activated page' : 'round-effect page')} onClick={() => changePage(number)}>{number} </div>)
    });

  return (
    <div className="flex-container page-container">
      <div className="paginate-ctn">
        <div className="round-effect page" onClick={data.hasPrevious ? () => changePage('prev') : null}> &lsaquo; </div>
        {items}
        <div className="round-effect page" onClick={data.hasNext ? () => changePage('next') : null}> &rsaquo; </div>
      </div>
    </div>);
}

Pagination.propTypes = {
  currentPage: PropTypes.number.isRequired,
}
export default Pagination;
