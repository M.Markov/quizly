import React, { useState, useContext } from 'react';
import AuthContext from '../../context/AuthContext';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Accordion, AccordionDetails, AccordionSummary, Button, AccordionActions, Typography } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import PropTypes from 'prop-types';
import AlertDialog from '../AlertDialog/AlertDialog';
import { useHistory, withRouter } from 'react-router-dom';
import { roles, SERVER_URL } from '../../common/constants';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Container from 'react-bootstrap/Container';
import { timeFormat } from '../../helpers/helpers';
const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: 50,
    },
    paper: {
        padding: theme.spacing(1),
        textAlign: 'center',
    },
    card: {
        height: 100
    },
    cardHeader: {
        color: '#364150',
        align: 'center',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '33.33%',
        flexShrink: 0,
    },
    headingBox: {
        borderBottom: 'solid',
        borderBottomWidth: 0.1,
        borderBottomColor: 'lightGrey',
    },
    startButton: {
        position: "absolute",
        right: 5,
    },
    historyButton: {
        position: "absolute",
        right: 10,
    }
}));

const CategoryQuizzes = (props) => {

    const classes = useStyles();
    const [expanded, setExpanded] = useState(false);
    const { user } = useContext(AuthContext);
    const {quizzes, category} = props;
    const [alert, setAlert] = useState({open:false, msg: null});
    const {categoryId} = props.match.params;
    const [quiz, setQuiz] = useState(null);
    const history = useHistory();
    const handleChange = (panel) => (event, isExpanded) => {
      setExpanded(isExpanded ? panel : false);
    };

    const solveButtonHandler = (quizId) => {
      if(user.role ===  roles.ADVANCED_USER_ROLE) {
        return history.push(`/categoties/${categoryId}/quizzes/${quizId}`);
      }
      setQuiz(quizId);
      setAlert({open:true, msg: 'You have only one attempt! Are you sure you want to do this?'});
    };

    const confirmButtonHandler = () => {
      setAlert({open:false, msg: null});
      return history.push(`/categoties/${categoryId}/quizzes/${quiz}`);
    };

    const showHistoryPage = (quizId) => {
      return history.push(`/categoties/${categoryId}/quizzes/${quizId}/history?page=1&sort=date_desc`);
    }

    return (
      <>
        <Box className={classes.root} pt={2} display='flex' alignItems='center' justifyContent='center'>
          <Box container spacing={1} md={6} width={700}>
            <Jumbotron className="myJumbotron" fluid style={{ backgroundImage: `url(${SERVER_URL}${category.cover_path })`}}>
              <Container style={{color: 'black', fontWeight: 'bold'}}>
                <h1>{category.name}</h1>
                <p>{category.description}</p>
              </Container>
            </Jumbotron>
            <AlertDialog alert={alert} close={confirmButtonHandler}>
              <Button onClick={() => setAlert({open:false, msg: null})} color="primary">Take me back</Button>
            </AlertDialog>
            {quizzes.map(quiz => {
                    return (
                      <Accordion key={quiz.id} expanded={expanded === `panel${quiz.id}`} onChange={handleChange(`panel${quiz.id}`)}>
                        <AccordionSummary
                          className={classes.headingBox}
                          expandIcon={<ExpandMoreIcon />}
                          aria-controls={`panel${quiz.id}bh-content`}
                          id={`panel${quiz.id}bh-header`}
                        >
                          <Typography>{quiz.name}</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                          <Typography style={{ paddingTop: 10 }}>
                            Quiz Duration: {timeFormat(quiz.duration)} min
                          </Typography>
                        </AccordionDetails>
                        <AccordionActions>
                          <Button disabled={quiz.isSolved} size="small" onClick={() => solveButtonHandler(quiz.id)}>{quiz.isSolved ? 'Solved' : 'Solve'}</Button>
                          {user.role ===  roles.ADVANCED_USER_ROLE ?
                            <Button size="small" disabled={!quiz.hasHistory} color="primary" onClick={() => showHistoryPage(quiz.id)}>
                              {quiz.hasHistory ? 'View history' : 'No history'}
                            </Button> : null}
                        </AccordionActions>
                      </Accordion>
                    )
                })}
            {props.children}
          </Box>
        </Box>
      </>
    )
};

CategoryQuizzes.propTypes = {
    quizzes: PropTypes.array.isRequired,
    category: PropTypes.array.isRequired,
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired,
    match: PropTypes.object.isRequired
}

export default withRouter(CategoryQuizzes);
