import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { capitalize, timeFormat } from '../../helpers/helpers.js';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Container from 'react-bootstrap/Container';

const useStyles = makeStyles({
    table: {
        maxWidth: 750,
    },
});

export default function UserHistory({data}) {
    const classes = useStyles();

    if(!data?.[0]) {
      return (
        <Jumbotron fluid>
          <Container>
            <h1>No history yet!</h1>
            <p>Dont waste your time. Take  advantage of our amazing quiz app! </p>
          </Container>
        </Jumbotron>
      )
    }
    const colums = Object.keys(data[0]).filter(el => el !== 'id');
     const rowsData = data.map(row => {
        const currentRow = colums.map( el => {
            return (el === 'name' 
            ? (<TableCell key={row.name} component="th" scope="row">{row.name}</TableCell>)
            : ( <TableCell key={row[el]} align="center">{el === 'duration' ? timeFormat(row.duration) + ' min' : row[el]}</TableCell>))});
        return (
          <>
            <TableRow key={row.id}>
              {currentRow}
            </TableRow>
          </>
        )
    })


    return (
      <TableContainer component={Paper} className={classes.table}>
        <Table aria-label="simple table">
          <TableHead>
            <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
              LAST FIVE QUIZZES HISTORY
            </Typography>
            <TableRow>
              {colums.map(el => <TableCell key={el}>{capitalize(el)}</TableCell>)}
            </TableRow>
          </TableHead>
          <TableBody>
            {rowsData}
          </TableBody>
        </Table>
      </TableContainer>
    );
}

UserHistory.propTypes = {
  data: PropTypes.array.isRequired
}

