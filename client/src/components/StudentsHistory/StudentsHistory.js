import React from 'react';
import PropTypes from 'prop-types';
import CollapsibleTable from '../CollapsibleTable/CollapsibleTable'
import {Grid} from '@material-ui/core';
import Pagination from '../Pagination/Pagination';
import ErrorAlert from '../AppError/ErrorAlert';
import SearchName from '../SearchName/SearchName';

const StudentsHistory = (props) => {
  if(props.data.message) {
    return (
      <>
        <SearchName setSearchParams={props.setQuerryParams} />
        <ErrorAlert message='No users with that name found!' />
        <Pagination data={props.data} change={props.setQuerryParams} />
      </>
    )
  }

  return (
    <Grid alignItems='center' justifyContent='center'>
      {props.data ? <CollapsibleTable data={props.data.history} setSearchParams={props.setQuerryParams} /> : null}
      <Pagination data={props.data} change={props.setQuerryParams} />
    </Grid>
  )
};

StudentsHistory.propTypes = {
  data: PropTypes.object.isRequired,
  setQuerryParams: PropTypes.func.isRequired,
  message: PropTypes.string.isRequired,
};

export default StudentsHistory;
