import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import {  Container } from '@material-ui/core';
import QuizHistorySettings from '../QuizHistorySettings/QuizHistorySettings.js'
import style from './style';
import {timeFormat} from '../../../helpers/helpers';
import { useHistory } from 'react-router-dom';
import useCustomQueryParams from '../../../custom-hooks/useCustomQueryParams';
import Paginanation from '../../Pagination/Pagination'

const useStyles = makeStyles((theme) => style(theme));

export default function QuizHistory({quizHistoryData, searchHandler}) {
  const classes = useStyles();
  const history = useHistory();
  const { page, sort } = useCustomQueryParams();
  const searchParams =  window.location.search;
  const rowsPerPage = 10;
  const quizHistory = quizHistoryData.records;
  const columns = Object.keys(quizHistory?.[0]).filter(el => el !== 'id');
  console.log(quizHistoryData);
  const setQuerryParams = (parameter, value) => {
    let currentUrlParams = new URLSearchParams(history.location.search);
    currentUrlParams.set(parameter, value);
    history.push({ search: '?' + currentUrlParams.toString() });
  }
 
  const handleRequestSort = (event, property) => {
    const [orderBy, order ] = sort.slice().split('_');
    const isAsc = orderBy === property && order === 'asc';
    const sortValue = (isAsc ? property + '_desc' :  property +'_asc');
    setQuerryParams('sort',sortValue);
    searchHandler(searchParams);
  };

  const emptyRows = Math.abs(quizHistory.length - rowsPerPage);

  return (
    <Container>
      <Paper className={classes.paper}>
        <Toolbar>
          <Typography className={classes.title} variant="h4" id="tableTitle" component="div">
            Attempts history
          </Typography>
        </Toolbar>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            aria-label="enhanced table"
          >
            {sort ? <QuizHistorySettings
              columns={columns}
              classes={classes}
              sort={sort}
              onRequestSort={handleRequestSort}
                    /> : null}
            <TableBody>
              {quizHistory
                .map((row) => {
                  return (
                    <TableRow
                      key={row.id}
                    >
                      <TableCell component="th" id={row.id}>
                        {row.username}
                      </TableCell>
                      <TableCell align="right">{row.name}</TableCell>
                      <TableCell align="right">{row.points}</TableCell>
                      <TableCell align="right">{timeFormat(row.time)}</TableCell>
                      <TableCell align="right">{row.date}</TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )} 
            </TableBody>
          </Table>
        </TableContainer>
        {page > 1 ? <Paginanation currentPage={page} maxPages={quizHistoryData.totalPages} change={setQuerryParams} /> : null}
      </Paper>
    </Container>
  );
}

QuizHistory.propTypes = {
  quizHistoryData: PropTypes.object.isRequired,
  searchHandler: PropTypes.func.isRequired,
};
