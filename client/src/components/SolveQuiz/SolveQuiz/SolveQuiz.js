import React, { useState, useEffect, useRef, useCallback} from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import { Typography, CssBaseline, Grid, CardContent, Card } from '@material-ui/core';
import cloneDeep from 'lodash/cloneDeep';
import { makeStyles } from '@material-ui/core/styles';
import ShowMultipleChoiceQuestion from '../ShowMiltipleChoiceQuestion/ShowMultipleChoiceQuestion';
import ShowSingleChoiceQuestion from '../ShowSingleChoiceQuestion/ShowSingleChoiceQuestion';
import PropTypes from 'prop-types';
import { timeFormat } from '../../../helpers/helpers';
import style from './style';

const useStyles = makeStyles((theme) => style(theme));


const SolveQuiz = ({ quizContent, submitQuizAttempt }) => {
  const classes = useStyles();
  const [isFormValid, setIsFormValid] = useState(false);
  const [time, setTime] = useState(+quizContent.duration);
  const secondsPassed = useRef(0);
  const [attemptAnswers, setAttemptAnswers] = useState(quizContent.questions.reduce((acc, question) => {
    const { questionId } = question;
    return acc.set(questionId, { questionId, answers: [] });
  }, new Map));

  const history = useHistory();

  const handleSubmit = useCallback((event) => {
    event ? event.preventDefault() : '';
    const data = { time: secondsPassed.current, questions: [...attemptAnswers.values()] };
    submitQuizAttempt(data);
  },[attemptAnswers]);

  useEffect(() => {
    const eventHandler = (event) => {
      event.preventDefault();
      event.returnValue = "";
    };
    window.addEventListener('beforeunload', eventHandler)
    return () => {
      window.removeEventListener('beforeunload', eventHandler)
    }
  }, []);


  useEffect(() => {
    return history.listen((location) => {
      handleSubmit();
    })
  }, [history]);

  useEffect(() => {
    if (time > 1) {
      const timeout = setTimeout(() => {
        const timeLeft = time - 1;
        secondsPassed.current++;
        setTime(timeLeft);
      }, 1000);
      return () => {
        clearTimeout(timeout);
      }
    } else {
      handleSubmit();
    }
  }, [time]);

  const validateForm = useCallback((map) => {
    const formInvalid = [...map.keys()].every(key => map.get(key).answers.length > 0);
    setIsFormValid(formInvalid);
  },[]);

  const singleChoiceHandler = useCallback((event, questionId) => {
    const newAttemptAnswers = cloneDeep(attemptAnswers);
    newAttemptAnswers.get(questionId).answers.toString() === event.target.value
      ? newAttemptAnswers.set(questionId, { questionId, answers: [] })
      : newAttemptAnswers.set(questionId, { questionId, answers: [+event.target.value] });
    validateForm(newAttemptAnswers);
    return setAttemptAnswers(newAttemptAnswers);
  },[attemptAnswers]);

  const multipleChoiceHandler = useCallback((event, questionId) => {
    const newAttemptAnswers = cloneDeep(attemptAnswers);
    if (event.target.checked) {
      newAttemptAnswers.get(questionId).answers.push(+event.target.id);
    } else {
      const newAnswers = newAttemptAnswers.get(questionId).answers.filter(id => id !== +event.target.id);
      newAttemptAnswers.set(questionId, { questionId, answers: newAnswers });
    }
    validateForm(newAttemptAnswers);
    return setAttemptAnswers(newAttemptAnswers);
  },[attemptAnswers]);



  return (
    <Container component="main">
      <CssBaseline />
      <div className={classes.myPaper}>
        <Typography component="h1" variant="h3">
          {quizContent.name}
        </Typography>
        <Typography variant="subtitle1" gutterBottom>
          Time left: {timeFormat(time)}
        </Typography>
        <form className={classes.myForm} onSubmit={handleSubmit}>
          <Grid container spacing={2} style={{ display: 'flex', justifyContent: 'center' }}>
            {quizContent.questions.map((question) => (
              <Card className={classes.myCard} key={question.id}>
                <CardContent>
                  <Grid className={classes.formControl}>
                    {question.type === 'single' ? <ShowSingleChoiceQuestion question={question} handler={singleChoiceHandler} />
                      : <ShowMultipleChoiceQuestion question={question} handler={multipleChoiceHandler} />}
                  </Grid>
                </CardContent>
              </Card>))}

            <Button
              className={classes.mySubmitButton}
              disabled={!isFormValid}
              variant="contained"
              color="primary"
              size="large"
              type="submit"
            >SUBMIT ATTEMPT
            </Button>
          </Grid>
        </form>
      </div>
    </Container>
  )
};

SolveQuiz.propTypes = {
  quizContent: PropTypes.object.isRequired,
  submitQuizAttempt: PropTypes.func.isRequired
}
export default SolveQuiz;
