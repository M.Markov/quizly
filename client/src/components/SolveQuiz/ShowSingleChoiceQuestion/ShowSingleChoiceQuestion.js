import React, {useState} from 'react';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Radio from '@material-ui/core/Radio';
import PropTypes from 'prop-types';

const ShowSingleChoiceQuestion = ({question, handler}) => {
  const [value, setValue] = useState('');
  
  const handleChange = (event) => {
    value == event.target.value
    ? setValue(null)
    : setValue(event.target.value);
    handler(event, question.questionId)
  };

    return (
      <>
        <FormControl component="fieldset">
          <FormLabel component="legend">{question.questionText}</FormLabel>
          <RadioGroup aria-label="gender" name="gender1" value={value} onClick={handleChange}>
            {question.answers.map(answer => {
              return(
                <FormControlLabel key={answer.answerId} value={`${answer.answerId}`} control={<Radio />} label={answer.answerText} />
              )
            })}
          </RadioGroup>
          <FormHelperText>Please select one.</FormHelperText>
        </FormControl>
      </>
    )
};

export default ShowSingleChoiceQuestion;

ShowSingleChoiceQuestion.propTypes = {
    question: PropTypes.object.isRequired,
    handler: PropTypes.func.isRequired,
}
