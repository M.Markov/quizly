import React from 'react';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';
import PropTypes from 'prop-types';

const ShowMultipleChoiceQuestion = ({question, handler}) => {

    return (
      <FormControl key={question.questionId} component="fieldset">
        <FormLabel component="legend">{question.questionText}</FormLabel>
        <FormGroup>
          {question.answers.map(answer => {
                return (
                  <FormControlLabel
                    key={answer.answerId}
                    control={<Checkbox id={answer.answerId} onChange={(event) =>handler(event,question.questionId)} />}
                    label={answer.answerText}
                  />)})}
        </FormGroup>
        <FormHelperText>Please select one or more.</FormHelperText>
      </FormControl>
    )
};

export default ShowMultipleChoiceQuestion;

ShowMultipleChoiceQuestion.propTypes = {
    question: PropTypes.object.isRequired,
    handler: PropTypes.object.isRequired,
}
