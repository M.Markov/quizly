import React from 'react';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Row from '../Leaderboard/Row';
import SearchName from '../SearchName/SearchName';
import ErrorAlert from '../AppError/ErrorAlert';
import { capitalize } from '../../helpers/helpers.js';

const CollapsibleTable = (props) => {
  if (props.data.length < 1) {
      return (
        <>
          <SearchName setSearchParams={props.setSearchParams} />
          <ErrorAlert message='No users with that name found!' />
        </>
      )
  }
  
  const rows =  props.data ? Object.values(props.data) : null;
  const cols = props.data ? Object.keys(props.data[0]).filter(el => [ 'Username', 'Rank', 'category', 'points' ].includes(el)) : null;
  
  return(
    <>
      <SearchName setSearchParams={props.setSearchParams} />
      <TableContainer component={Paper} align='justify' style={{ maxWidth: 500, padding: {top: 0, right: 1000} }}>
        <Table aria-label="collapsible table" className="table">
          <TableHead>
            <TableRow>
              <TableCell />{cols.map(el => <TableCell key={el} align="left">{capitalize(el)}</TableCell>)}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <Row key={row.id} row={row} />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  )
}

CollapsibleTable.propTypes = {
  data: PropTypes.object.isRequired,
  setSearchParams: PropTypes.func.isRequired,
};

export default CollapsibleTable;
