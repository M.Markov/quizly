export const BASE_URL = 'http://localhost:3000';

export const SERVER_URL = 'http://localhost:3000/public/';

export const roles = {
    DEFAULT_USER_ROLE: 'student',
    ADVANCED_USER_ROLE: 'teacher',
};


