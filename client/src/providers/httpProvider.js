import storageProvider from "./storageProvider";

const getAll = (...urls) => {

  const request = {
    headers: {
      Authorization: `Bearer ${storageProvider.getToken()}`,
    }
  };
  return Promise.all(urls.map(url => fetch(url, request)))
    .then(result => Promise.all(result.map(v => v.json())));
};

const get = (url) => {
  const token = storageProvider.getToken();

  return fetch(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }).then((res) => res.json());
};

const post = (url, body) => {
  const token = storageProvider.getToken();
  const request = {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json',
    },
  }
  if (body) request.body = JSON.stringify(body);

  return fetch(url, request)
    .then((res) => res.json());
};

const postFormData = (url, data) => {
  const formData = Object.keys(data).reduce((acc, el) => {
    acc.set(el, data[el]);
    return acc;
  }, new FormData());

  return fetch(url, {
    method: 'POST',
    headers: { Authorization: `Bearer ${storageProvider.getToken()}` },
    body: formData
  })
    .then(r => r.json())
}

const put = (url, body) => {
  const token = storageProvider.getToken();

  return fetch(url, {
    body: JSON.stringify(body),
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json',
    },
  }).then((res) => res.json());
};

const httpProvider = {
  get,
  getAll,
  post,
  postFormData,
  put
};

export default httpProvider;
