export const timeFormat = (time) => {
    const mins = Math.floor((time % 3600) / 60);
    const secs = Math.floor(time % 60);
    let result = "";
    result += "" + mins + ":" + (secs < 10 ? "0" : "");
    result += "" + secs;
    return result;
};

export const capitalize = (word) => {
    return word.charAt(0).toUpperCase() + word.slice(1)
};


export const validateInput = (object) => {

    const validators = {
        'text': value => {
            if (!value) {
                return 'Text is required!';
            }

            if (typeof value !== 'string' || value.trim().length < 5 || value.trim().length > 200) {
                return 'Text should be in the range of [5..200] charachers.';
            }

            return null;
        },
        'select': value => {
            if (!value) {
                return 'You must select an option!';
            }
            return null;
        },
        'number': value => {
            if (!value) {
                return 'Duration is required!';
            }

            if (isNaN(value) || value < 1 || value > 60) {
                return 'Duration should be in range [1..60] min';
            }

            return null;
        },
        'boolean': value => {
            if (typeof value !== 'boolean') {
                return 'Answer value is not correct.';
            }
            return null;
        },
        'type': value => {
            if (value !== 'single' && value !== 'multiple') {
                return 'Answer value is not correct.';
            }
            return null;
        },
        'questions': value => {
            if (!value.length) {
                return 'Questions are required!';
            }
            if (value.length < 2) {
                return 'Quiz must be a colletion of at least 2 questions!';
            }
            return null;
        },
        'answers': value => {
            if (value.every(answer => !answer.text.value || answer.text.value.trim().length < 1 || answer.text.value.trim().length > 300)) {
                return 'Answers are required!';
            }
            if (value.some(answer => !answer.text.value || answer.text.value.trim().length < 1 || answer.text.value.trim().length > 300)) {
                return 'Every answer must have text!';
            }
            if (value.every(answer => answer.value.value === false)) {
                return 'Every question must have at least one possible answer depending on the type!';
            }
            return null;
        },
    }

    const error = validators[object.type](object.value);
    object.touched = true;
    object.errorMsg = error;
    object.valid = !error;
    return error;
}
