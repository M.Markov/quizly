import errors from '../common/errors.js';
import { roles } from '../common/users.js';

const getQuizzes = quizzesData => {
  return async (pageNumber, categoryId, userId) => {
    const limit = 5;
    const offset = (pageNumber - 1) * limit;

    const quizzes = await quizzesData.getPage(limit, offset, categoryId, userId);
    if (!quizzes.totalPages) {
      return {
        error: errors.RECORD_NOT_FOUND('Quizzes not found!'),
        quizzes: null,
      };
    }

    return {
      error: null,
      quizzes: { ...quizzes, currentPage: pageNumber, hasNext: (offset + limit) < quizzes.count, hasPrevious: pageNumber > 1 },
    };
  };
};

const getQuizById = quizzesData => {
  return async (quizId, userId) => {

    const quiz = await quizzesData.getBy('id', quizId);

    if (!quiz) {
      return {
        error: errors.RECORD_NOT_FOUND('The quiz is not found!'),
        quiz: null,
      };
    }

    const attempt = await quizzesData.getAttempt(quizId, userId);

    if (attempt) {
      return {
        error: errors.OPERATION_NOT_PERMITTED('You had only one attempt!'),
        quiz: null,
      };
    }

    return { error: null, quiz };
  };
};

const addQuiz = quizzesData => {
  return async (data, categoryId, userId) => {

    const quiz = await quizzesData.getBy('name', data.name);

    if (quiz) {
      return {
        error: errors.DUPLICATE_RECORD('Quiz with the same name already exist!'),
        quiz: null,
      };
    }
    const dataResult = await quizzesData.add(data, categoryId, userId);

    if (dataResult?.error) {
      return {
        error: errors.OPERATION_FAILED( 'Adding quiz failed!'),
        quiz: null,
      };
    }

    return { error: null, quiz: await quizzesData.getBy('id', dataResult.id) };
  };
};

const getQuizHistory = quizzesData => {
  //page=1&sort=date_asc&username=mari
  return async (quizId, filter) => {

    const quiz = await quizzesData.getBy('id', quizId);

    if (!quiz) {
      return {
        error: errors.RECORD_NOT_FOUND('The quiz is not found!'),
        history: null,
      };
    }

    let history = null;
    const limit = 10;
    const offset = (filter.page - 1) * limit;

    if(Object.keys(filter).length === 1 && filter.page) {
     history = await quizzesData.getHistory(quizId, limit, offset);
    } else {
      const clientSearch = {...filter};
    if(clientSearch.sort) {
      const [sortBy, order] = clientSearch.sort.slice().split('_');
      clientSearch.sortBy = sortBy;
      clientSearch.order = order;
    }
    const set = new Set(['username', 'sortBy', 'order']);
    const searchParams = Object.entries(clientSearch).filter(x => set.has(x[0]));
    
    history = await quizzesData.getHistoryBy(quizId, searchParams, limit, offset);
    }


    if (!history.totalPages) {
      return {
        error:errors.RECORD_NOT_FOUND( 'No history found!'),
        history: null,
      };
    }

    return {
      error: null,
      history: { ...history, currentPage: filter.page, hasNext: (offset + limit) < history.totalPages, hasPrevious: filter.page > 1 },
    };
  };
};


const submitQuizAttempt = quizzesData => {
  return async (quizId, user, data) => {

    const quiz = await quizzesData.getBy('id', quizId);

    if (!quiz) {
      return {
        error: errors.RECORD_NOT_FOUND('The quiz is not found!'),
        score: null,
      };
    }

    const attempt = await quizzesData.getAttempt(quizId, +user.id);

    if (attempt) {
      return {
        error: errors.OPERATION_NOT_PERMITTED('You had only one attempt!'),
        score: null,
      };
    }

    const { time, questions } = data;
    const questionsWithAnswers = await quizzesData.getAnswers(quizId);
    const submittedQuestions = questions.reduce(( acc, question) => {
      const { questionId, answers } = question;
      if (!acc.has(questionId)) {
        acc.set(questionId, answers);
      }
      return acc;
    }, new Map());
    const score = questionsWithAnswers.reduce((acc, q) => {
      if (submittedQuestions.has(q.questionId) && submittedQuestions.get(q.questionId).slice().sort().join(',') === q.answers.slice().sort().join(',')) {
        acc.attempt += q.points;
      }
      acc.totalPoints += q.points;
      return acc;
    }, {attempt: 0, totalPoints: 0});

    if (user.role === roles.DEFAULT_USER_ROLE) {
      const settledAttempt = await quizzesData.setAttempt(quizId, +user.id, time, score.attempt);

      if (settledAttempt?.error) {
        return { error: errors.OPERATION_FAILED('Quiz submitting failed!'), score: null };
      }
    }

    return { error: null, score: {...score} };
  };

};

export default {
  getQuizzes,
  getQuizById,
  addQuiz,
  getQuizHistory,
  submitQuizAttempt,
};
