import errors from '../common/errors.js';


const getCategories = categoriesData => {
  return async () => {
    const categories = await categoriesData.getAll();

    if (!categories.count) {
      return {
        error: errors.RECORD_NOT_FOUND('No categories found!'),
        categories: null,
      };
    }
    return { error: null, categories };
  };
};

const addCategory = categoriesData => {
  return async (name, description, path, userId) => {
    
    const existingCategory = await categoriesData.getBy('name', name);

    if (existingCategory) {
      return {
        error: errors.DUPLICATE_RECORD('The category already exist!'),
        category: null,
      };
    }

    const category = await categoriesData.add(name,description, path, userId);

    return { error: null, category };
  };

};


const getCategory = categoriesData => {
  return async (id) => {
    
    const category = await categoriesData.getBy('id', id);
    if (!category) {
      return {
        error: errors.RECORD_NOT_FOUND('Category not found!'),
        category: null,
      };
    }
    return { error: null, category };
  };

};

export default {
  getCategories,
  addCategory,
  getCategory,
};
