import errors from '../common/errors.js';
import bcrypt from 'bcrypt';
import { roles } from '../common/users.js';

const getAllUsers = usersData => {
  return async () => {
    const users = await usersData.getAll();

    if (!users) {
      return {
        error: errors.RECORD_NOT_FOUND('No users found!'),
        user: null,
      };
    }

    return { error: null, users: users };
  };
};

const getUserById = usersData => {
  return async (id, user) => {
    if (id !== user.id) {
      return {
        error: errors.OPERATION_NOT_PERMITTED('You can get only your own details'),
        user: null,
      };
    }

    const userDetails = user.role === roles.DEFAULT_USER_ROLE ?
      await usersData.getStudentBy('id', id)
      :
      await usersData.getTeacherBy('id', id);

    if (!userDetails) {
      return {
        error: errors.RECORD_NOT_FOUND('User not found!'),
        user: null,
      };
    }

    return { error: null, user: userDetails };
  };
};

const signInUser = usersData => {
  return async (username, password) => {
    const user = await usersData.getUserBy('username', username);

    if (!user || !(await bcrypt.compare(password, user.password))) {
      return {
        error: errors.INVALID_SIGNIN('Invalid username/password'),
        user: null,
      };
    }
    return { error: null, user: user };
  };
};

const createUser = usersData => {
  return async (userCreate) => {
    const { username, password, firstname, lastname } = userCreate;

    const existingUser = await usersData.getUserBy('username', username);

    if (existingUser) {
      return {
        error: errors.DUPLICATE_RECORD('Username is not available'),
        user: null,
      };
    }

    const passwordHash = await bcrypt.hash(password, 10);
    const user = await usersData.createUser(username, passwordHash, firstname, lastname, roles.DEFAULT_USER_ROLE);

    return { error: null, user: user };
  };
};

const getHistory = usersData => {
  return async (userId, user, query) => {
    let limit = 5;
    const page = +query.page || 1;

    if(query.page !== 'undefined') {
      limit = 10;
    }

    const offset = (page - 1) * limit;

    if (userId !== user.id) {
      return {
        error: errors.OPERATION_NOT_PERMITTED('You can get only your own history'),
        user: null,
      };
    }

    const existingUser = await usersData.getUserBy('id', userId);

    if (!existingUser) {
      return {
        error: errors.RECORD_NOT_FOUND('User not found!'),
        user: null,
      };
    }

    if (user.role !== roles.DEFAULT_USER_ROLE) {
      const history = await usersData.getTeacherUploadHistory(userId);
      if (!history.length) {
        return {
          error: errors.RECORD_NOT_FOUND('History not found!'),
          history: null,
        };
      }
      return { error: null, history };
    }

    if (query.name) {
      
      const quizAttempt = await usersData.getStudentSolveHistoryByQuiz( userId, 'name', query.name, limit);
      
      if (!quizAttempt.history.length) {
        return {
          error: errors.RECORD_NOT_FOUND('No quiz with such name found in your history!'),
          history: null,
        };
      }
      return { 
        error: null, 
        history: quizAttempt, currentPage: page, hasNext: (offset + limit) < quizAttempt.count, hasPrevious: page > 1,
      };
    }

    const history = await usersData.getStudentSolveHistory(userId, limit, offset);


    if (!history.history.length) {
      return {
        error: errors.RECORD_NOT_FOUND('History not found!'),
        history: null,
      };
    }

    return {
      error: null,
      history: { ...history, currentPage: page, hasNext: (offset + limit) < history.count, hasPrevious: page > 1 },
    };
  };
};

const getLeaderboard = usersData => {
  return async (query) => {
    const page = +query.page || 1;
    const limit = 10;
    const offset = (page - 1) * limit;

    if (query.username) {
      const users = await usersData.getUserRankBy('username', query.username, limit, offset);
      
      if (!users) {
        return {
          error: errors.RECORD_NOT_FOUND('Users with searched name is not found!'),
          users: null,
        };
      }

      return { error: null, users: { ...users, currentPage: page, hasNext: (offset + limit) < users.count, hasPrevious: page > 1 }};
    }

    const users = await usersData.getLeaderboard(limit, offset);

    if (!users.users.length) {
      return {
        error: errors.RECORD_NOT_FOUND('Users is not found!'),
        users: null,
      };
    }

    return {
      error: null,
      users: { ...users, currentPage: page, hasNext: (offset + limit) < users.count, hasPrevious: page > 1 },
    };
  };
};

export default {
  getAllUsers,
  getUserById,
  signInUser,
  createUser,
  getHistory,
  getLeaderboard,
};
