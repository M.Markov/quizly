import express from 'express';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import { validatorMiddleware, addQuizSchema } from '../validators/index.js';
import checkCategoryMiddleware from '../middlewares/check-category.js';
import { roles } from '../common/users.js';

const createQuizzesController = (quizzesService, quizzesData) => {

  const quizzesController = express.Router();
  quizzesController.use(authMiddleware);

  quizzesController
    .get('/:id/quizzes', checkCategoryMiddleware, async (req, res, next) => {
      const { id } = req.params;
      const page = req.query.params?.page || 1;
      const userId = req.user.id;
      const { error, quizzes } = await quizzesService.getQuizzes(quizzesData)(+page, +id, +userId);

      return error ? next(error) : res.status(200).send(quizzes);
    })
    .post('/:id/quizzes', roleMiddleware(roles.ADVANCED_USER_ROLE), checkCategoryMiddleware, validatorMiddleware(addQuizSchema),
      async (req, res, next) => {
        const categoryId = req.params.id;
        const data = req.body;
        const userId = req.user.id;
        const { error, quiz } = await quizzesService.addQuiz(quizzesData)(data, +categoryId, +userId);

        return error ? next(error) : res.status(201).send(quiz);
      })
    .get('/:id/quizzes/:quizId', checkCategoryMiddleware, async (req, res, next) => {
      const { quizId } = req.params;
      const userId = req.user.id;
      const { error, quiz } = await quizzesService.getQuizById(quizzesData)(+quizId, userId);

      return error ? next(error) : res.status(200).send(quiz);
    })
    .put('/:id/quizzes/:quizId', checkCategoryMiddleware, async (req, res, next) => {
      const { quizId } = req.params;
      const user = req.user;
      const data = req.body;
      const { error, score } = await quizzesService.submitQuizAttempt(quizzesData)(+quizId, user, data);

      return error ? next(error) : res.status(200).send(score);
    })
    .get('/:id/quizzes/:quizId/history', checkCategoryMiddleware, roleMiddleware(roles.ADVANCED_USER_ROLE),
      async (req, res, next) => {
        const { quizId } = req.params;
        const searchParams = req.query;
        const { error, history } = await quizzesService.getQuizHistory(quizzesData)(+quizId, searchParams);
        return error ? next(error) : res.status(200).send(history);
      });
  return quizzesController;
};
export default createQuizzesController;
