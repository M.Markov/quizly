import express from 'express';
import createToken from '../auth/create-token.js';
import { validatorMiddleware, loginSchema, createUserSchema } from '../validators/index.js';

const createAuthController = (usersService, usersData) => {

    const authController = express.Router();

    authController
        .post('/', validatorMiddleware(loginSchema), async (req, res, next) => {
            const { username, password } = req.body;
            const { error, user } = await usersService.signInUser(usersData)(username, password);

            if (error) {
                return next(error);
            }

            const payload = {
                sub: user.id,
                username: user.username,
                subFullname: user.full_name,
                role: user.role,
            };
            const token = createToken(payload);

            return res.status(200).send({ token: token });
        })
        .post('/signup', validatorMiddleware(createUserSchema), async (req, res, next) => {
            const data = req.body;
            const { error, user } = await usersService.createUser(usersData)(data);

            if (error) {
                return next(error);
            }

            const payload = {
                sub: user.id,
                username: user.username,
                role: user.role,
            };
            const token = createToken(payload);

            return res.status(200).send({
                token: token,
                user,
            });
        });
    return authController;
};
export default createAuthController;
