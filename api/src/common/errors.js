export default {
    RECORD_NOT_FOUND: (msg) => {
      return {
      code: 404,
      message: msg,
      };
    },
    DUPLICATE_RECORD: (msg) => {
      return {
      code: 409,
      message: msg,
      };
    }, 
    OPERATION_NOT_PERMITTED: (msg) => {
      return {
      code: 409,
      message: msg,
      };
    },
    INVALID_SIGNIN: (msg) => {
      return {
      code: 400,
      message: msg,
      };
    },
    OPERATION_FAILED: (msg) => {
      return {
      code: 409,
      message: msg,
      };
    },
  };