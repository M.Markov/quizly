export const roles = {
    DEFAULT_USER_ROLE: 'student',
    ADVANCED_USER_ROLE: 'teacher',
};