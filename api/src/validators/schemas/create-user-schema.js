const createUserSchema = {
  username: value => {
    if (!value) {
      return 'Username is required';
    }

    if (typeof value !== 'string' || value.trim().length < 3 || value.trim().length > 25) {
      return 'Username should be a string in range [3..25]';
    }

    return null;
  },
  password: value => {
    if (!value) {
      return 'Password is required';
    }

    if (typeof value !== 'string' || value.trim().length < 3 || value.trim().length > 25) {
      return 'Password should be a string in range [3..25]';
    }

    return null;
  },
  confirmPassword: value => {
    if (!value) {
      return 'Password confirm is required';
    }

    if (typeof value !== 'string' || value.trim().length < 3 || value.trim().length > 25) {
      if (value !== this.password) {
        return 'Confirm password should match password';
      }
    }

    return null;
  },
  firstname: value => {
    if (!value) {
      return 'Firstname is required';
    }

    if (typeof value !== 'string' || value.trim().length < 3 || value.trim().length > 25) {
      return 'Firstname should be a string in range [3..25]';
    }

    return null;
  },
  lastname: value => {
    if (!value) {
      return 'Lastname is required';
    }

    if (typeof value !== 'string' || value.trim().length < 3 || value.trim().length > 25) {
      return 'Lastname should be a string in range [3..25]';
    }

    return null;
  },
};

export { createUserSchema };