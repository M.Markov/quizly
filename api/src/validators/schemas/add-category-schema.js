const addCategorySchema = {
    name: value => {
      if (!value) {
        return 'Name is required!';
      }
  
      if (typeof value !== 'string' || value.trim().length < 3 || value.trim().length > 25) {
        return 'Name should be a string in range [3..25]';
      }
  
      return null;
    },
    description: value => {
      if (!value) {
        return 'Description is required!';
      }
  
      if (typeof value !== 'string' || value.trim().length < 3 || value.trim().length > 300) {
        return 'Description should be a string in range [3..300]';
      }
  
      return null;
    },
  };
  
  export { addCategorySchema };