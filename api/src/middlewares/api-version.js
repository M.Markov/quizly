const API_VERSION = '0.1.0';

export default (req, res, next) => {
  res.set('X-API-Version', API_VERSION);

  next();
};
