import categoriesData from '../data/categories-data.js';
import categoriesService from '../services/categories-service.js';

const checkCategoryMiddleware = async (req, res, next) => {
    const { id } = req.params;

    const { error } = await categoriesService.getCategory(categoriesData)(+id);
    
    return error ? next(error) : next();
};

export default checkCategoryMiddleware;