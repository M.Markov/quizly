/* eslint-disable no-unused-vars */
const handleErrorsMiddleware = (err, req, res, next) => {

    if(err.code && err.message) {
        return res.status(err.code).send({message: err.message});
    }

    return res.status(500).send({
      message: 'An unexpected error occurred!',
    });
  };
  export default handleErrorsMiddleware;