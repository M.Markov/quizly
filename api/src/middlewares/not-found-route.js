const notFoundRouteMiddleware = ( req, res) => {

    return res.status(404).send({message: 'Resource not found'});
};
  export default notFoundRouteMiddleware;