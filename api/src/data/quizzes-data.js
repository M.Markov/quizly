/* eslint-disable no-unused-expressions */
/* eslint-disable no-loop-func */
/* eslint-disable no-unused-vars */
import pool from './pool.js';


const getPage = async (limit, offset, categoryId, userId) => {
    const sql1 = `
    SELECT q.id, q.name, q.duration, IF(attempts.id,true,false) AS isSolved,IF(w.hasHistory, true, false) as hasHistory FROM quizzes AS q
    LEFT JOIN attempts
    ON q.id = attempts.quizzes_id AND attempts.users_id = ?
    LEFT JOIN (SELECT attempts.quizzes_id AS hasHistory
    FROM  attempts GROUP BY quizzes_id) as w on w.hasHistory = q.id
    where q.categories_id = ?
    ORDER BY isSolved
    LIMIT ? OFFSET ?;
    `;

    const sql2 = `
    SELECT COUNT(*) as totalPages FROM quizzes
    WHERE categories_id = ?;
    `;

    const quizzes = await pool.query(sql1, [userId, categoryId, limit, offset]);
    const totalPages = await pool.query(sql2, [categoryId]);

    return { quizzes, totalPages: Math.ceil(totalPages[0]?.totalPages/limit) };
};

const getBy = async (column, value) => {
    const sql = `
    SELECT u.id, u.name, u.duration, q.id as questionId, q.text as questionText, q.points, t.name as type,
    a.id as answerId, a.text as answerText FROM answers a
    LEFT JOIN questions q ON a.questions_id = q.id
    LEFT JOIN types t ON q.types_id = t.id
    LEFT JOIN quizzes u ON q.quizzes_id = u.id
    WHERE u.${column} = ?;
    `;
    const result = await pool.query(sql, [value]);

    if (!result.length) {
        return null;
    }

    const quiz = { id: result[0].id, name: result[0].name, duration: result[0].duration };
    const questionsData = result.map(row => {
        const { id, name, duration, ...rest } = row;
        return rest;
    });
    const map = questionsData.reduce((acc, row) => {
        const { questionId, questionText, type, points, duration, ...rest } = row;
        if (!acc.has(questionId)) {
            acc.set(questionId, {
                questionId, questionText, type, points, duration, answers: [],
            });
        }
        acc.get(questionId).answers.push(rest);
        return acc;

    }, new Map());
    return { ...quiz, questions: [...map.values()] };

};

const add = async (data, categoryId, userId) => {

    const { name, duration, questions } = data;
    const connection = await pool.getConnection();
    try {
        await connection.beginTransaction();
        const sql1 = `
        INSERT INTO quizzes(categories_id, users_id, name, duration)
        VALUES (?,?,?,?);
        `;
        const newQuiz = await connection.query(sql1, [categoryId, userId, name, duration]); 
        for (const question of questions) {
            const { text, type, points, answers } = question;
            const sql2 = `
            INSERT INTO questions(quizzes_id, text, points,types_id)
            VALUES (?,?,?,(SELECT id from types WHERE name = ?));
            `;
            const newQestion = await connection.query(sql2, [newQuiz.insertId, text, points, type]);
            let values = [];
            const answersArr = answers.map( answer => {
                values.push('(?,?,?)');
                const filteredAsnwer = {'text': answer.text, 'value': answer.value};
                return [newQestion.insertId,...Object.values(filteredAsnwer)];
            }).flat();
            values = values.join(', ');
            const sql3 = `
            INSERT INTO answers(questions_id, text, is_correct)
            VALUES ${values};
            `;
            await connection.query(sql3, answersArr);

        }
        await connection.commit();
        return { error: null, id: newQuiz.insertId };

    } catch (error) {
        await connection.rollback();
        return { error: true, id: null };
    } finally {
        await connection.release();
    }
};

const getAttempt = async (quizId, userId) => {
    const sql = `
    SELECT * from attempts 
    WHERE quizzes_id = ? AND users_id = ?;
    `;

    const result = await pool.query(sql, [quizId, userId]);
    return result?.[0];
};

const setAttempt = async (quizId, userId, time, score) => {
    const sql = `
    INSERT INTO attempts (quizzes_id,users_id, time, points)
    VALUES (?,?,?,?);
    `;

    const result = await pool.query(sql, [quizId, userId, time, score]);
    if(result.affectedRows < 1) {
        return {error: true, id: null};
    }
    return {error: null, id: result.insertId};
};


const getHistory = async (quizId, limit, offset) => {
    const sql1 = `
    SELECT  quizzes_id as id, u.username,concat(u.first_name,' ',u.last_name) name, a.points, a.time, DATE_FORMAT(a.date, '%d/%m/%Y') as date FROM attempts a
    LEFT JOIN users u ON a.users_id = u.id
    WHERE quizzes_id = ?
    ORDER BY a.date
    LIMIT ? OFFSET ?;
    `;

    const sql2 = `
    SELECT COUNT(*) as count FROM attempts
    WHERE quizzes_id = ?;
    `;

    const records = await pool.query(sql1, [quizId, limit, offset]);
    const totalPages = await pool.query(sql2, [quizId]);

    return { records, totalPages: Math.ceil(totalPages[0]?.count/limit) };
};

const getHistoryBy = async (quizId, searchParams, limit, offset) => {
    const sql1 = `
    SELECT  quizzes_id as id, u.username,concat(u.first_name,' ',u.last_name) name, a.points, a.time, DATE_FORMAT(a.date, '%d/%m/%Y') as date FROM attempts a
    LEFT JOIN users u ON a.users_id = u.id
    WHERE quizzes_id = ? 
    `;

    const sqlObj = searchParams.reduce((acc, el) => {
        switch (el[0]) {
            case 'username':
              acc.sql2=(`AND username LIKE '%${el[1]}%'`);
              break;
            case 'sortBy':
                el[1] !== 'date' ? acc.sql3=(`ORDER BY ${el[1]}`) : acc.sql3=(' ORDER BY a.date');
                break;
            default:
                acc.sql4=(`${el[1]}`);
        }
        return acc;
    },{sql2:'', sql3:'', sql4:'' });

    const sql2 = Object.values(sqlObj).join(' ');
    const sql3 = ' LIMIT ? OFFSET ?';

    let sql4 = `
    SELECT COUNT(*) as count FROM attempts AS a
    LEFT JOIN users u ON a.users_id = u.id
    WHERE quizzes_id = ? 
    `;
    
    sqlObj.sql2 ? sql4 += sqlObj.sql2 +';' : sql4 += ';';
    const records = await pool.query(sql1 + sql2 + sql3, [quizId, limit, offset]);
    const totalPages = await pool.query(sql4, [quizId]);
    return { records, totalPages: Math.ceil(totalPages[0]?.count/limit) };
};

const getAnswers = async (quizId) => {
    const sql = `
    SELECT q.id as questionId, q.points,
    a.id as answerId, a.is_correct as isCorrect FROM answers a
    LEFT JOIN questions q ON a.questions_id = q.id
    WHERE q.quizzes_id = ?;
    `;

    const result = await pool.query(sql, [quizId]);
    const map = result.reduce((map, row) => {
        const { questionId, points, answerId, isCorrect } = row;
        if (!map.has(questionId)) {
            map.set(questionId, {questionId, points, answers: []});
        }
        if(isCorrect) {
            map.get(questionId).answers.push(answerId);
        }
        return map;
    }, new Map());
    return [...map.values()];
};

export default {
    getPage,
    getBy,
    add,
    getAttempt,
    setAttempt,
    getHistory,
    getHistoryBy,
    getAnswers,
};