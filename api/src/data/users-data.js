import pool from './pool.js';

const getAll = async () => {
  const sql = `
    SELECT u.id, u.username, u.first_name, u.last_name, r.type as role
    FROM users u
    JOIN roles r ON u.roles_id = r.id; 
  `;

  const sql2 = `
    SELECT COUNT(*) as count FROM users;
  `;

  const users = await pool.query(sql);
  const count = await pool.query(sql2);

  return { users: users, count: count[0].count };
};

const getUserBy = async (column, value) => {
  const sql = `
    SELECT u.id, u.username, u.password, CONCAT(u.first_name,' ',u.last_name) full_name, r.type as role
    FROM users u
    LEFT JOIN roles r ON u.roles_id = r.id 
    LEFT JOIN attempts a ON u.id=a.users_id
    WHERE u.${column} = ?
  `;

  const result = await pool.query(sql, [value]);
  return result[0];
};

const createUser = async (username, password, firstname, lastname, role) => {
  const sql = `
    INSERT INTO users(username, first_name, last_name, password, roles_id)
    VALUES (?,?,?,?,(SELECT id FROM roles WHERE type = ?));
  `;

  const result = await pool.query(sql, [username, firstname, lastname, password, role]);
  return {
    id: result.insertId,
    username: username,
  };
};

const getStudentSolveHistory = async (id, limit, offset) => {

  const sql = `
    SELECT u.id, 
    c.name AS category, 
    q.name AS quiz, 
    a.points, 
    SEC_TO_TIME(a.time) AS time
    FROM users u
    LEFT JOIN attempts a
    ON u.id = a.users_id
    LEFT JOIN quizzes as q
    ON a.quizzes_id = q.id
    LEFT JOIN categories as c
    ON q.categories_id = c.id
    WHERE a.users_id = ?
    ORDER BY a.id DESC
    LIMIT ? OFFSET ?;
  `;

  const sql2 = `
    SELECT COUNT(DISTINCT id) AS count
    FROM attempts
    WHERE users_id = ?;
  `;
  
  const history = await pool.query(sql, [+id, limit, offset]);
  const count = await pool.query(sql2,[id]);
  return { history, count: Math.ceil(count[0]?.count / limit) };
};

const getStudentSolveHistoryByQuiz = async (id, column, value, limit) => {
  const sql = `
  SELECT a.id, 
    c.name AS category, 
    q.name AS quiz, 
    a.points, 
    SEC_TO_TIME(a.time) AS time
  FROM attempts a
  LEFT JOIN quizzes as q
  ON a.quizzes_id = q.id
  LEFT JOIN categories as c
  ON q.categories_id = c.id
  WHERE a.users_id = ? AND q.${column} LIKE '%${value}%';
  `;

  const history = await pool.query(sql, [id]);
  const count = Math.ceil(history.length / limit);
  return { history, count: Math.ceil(count / limit) };
};

const getTeacherUploadHistory = async (id) => {
  const sql = `
    SELECT quiz.id, quiz.name,
    quiz.duration,
    c.name AS category,
    (SELECT COUNT(a.users_id) FROM attempts a
    WHERE a.quizzes_id=quiz.id) AS attempts
    FROM quizzes AS quiz
    JOIN categories c ON quiz.categories_id = c.id
    JOIN users u ON quiz.users_id = u.id
    WHERE u.id = ?
    ORDER BY quiz.id DESC
    LIMIT 5;
  `;

  return await pool.query(sql, [id]);
};

const getLeaderboard = async (limit, offset) => {
  const sql = `
    SELECT u.id, u.username Username, 
    CONCAT(u.first_name,' ',u.last_name) Fullname,
    SUM(points) AS Points,
    RANK() OVER (ORDER BY Points DESC) Rank
    FROM attempts AS a
    LEFT JOIN users AS u on a.users_id = u.id
    GROUP BY users_id
    ORDER BY Rank
    LIMIT ? OFFSET ?;
  `;

  const sql2 = `
    SELECT COUNT(DISTINCT users_id) AS count FROM attempts;
  `;

  const users = await pool.query(sql, [limit, offset]);
  const count = await pool.query(sql2);
  return { users, count: Math.ceil(count[0]?.count / limit) };
};

const getUserRankBy = async (column, value, limit, offset) => {
  const sql = `
    SELECT * FROM (SELECT u.id, u.username AS Username, 
    CONCAT(u.first_name,' ',u.last_name) Fullname,
    SUM(points) AS Points,
    RANK() OVER (ORDER BY Points DESC) Rank
    FROM attempts AS a
    LEFT JOIN users AS u ON a.users_id = u.id
    GROUP BY users_id) AS w
    WHERE w.${column} LIKE '${value}%'
    LIMIT ? OFFSET ?;
  `;

  const users = await pool.query(sql, [limit, offset]);

  return { users, count: Math.ceil(users.length / limit) };
};

const getStudentBy = async (column, value) => {
  const sql = `
    SELECT * FROM (SELECT u.username, u.id, w.totalPoints, w.quizzesCount, w.studentsRank,
    CONCAT(u.first_name,' ',u.last_name) fullName
    FROM users AS u
    Left join (SELECT u.id,
    SUM(points) AS totalPoints,
    COUNT(a.id) as quizzesCount,
    RANK() OVER (ORDER BY totalPoints DESC) studentsRank
    FROM users AS u
    LEFT JOIN attempts AS a ON a.users_id = u.id
    GROUP BY users_id) as w on w.id = u.id
    WHERE u.roles_id = (select id from roles where type = 'student')) AS r
    WHERE r.${column} = ?;
  `;

  const result = await pool.query(sql, [value]);
  return result[0];
};

const getTeacherBy = async (column, value) => {
  const sql = `
    SELECT u.id, u.username,
    CONCAT(u.first_name,' ',u.last_name) fullName,
    c.categoriesCount, q.quizzesCount
    FROM users AS u
    LEFT JOIN (SELECT users_id,
    count(*) AS categoriesCount FROM categories GROUP BY users_id) AS c ON c.users_id = u.id
    LEFT JOIN (SELECT users_id,
    count(*) AS quizzesCount FROM quizzes GROUP BY users_id) AS q ON q.users_id = u.id
    WHERE u.${column} = ?;
  `;

  const result = await pool.query(sql, [value]);
  return result[0];
};

export default {
  getAll,
  getUserBy,
  createUser,
  getStudentSolveHistory,
  getStudentSolveHistoryByQuiz,
  getTeacherUploadHistory,
  getLeaderboard,
  getUserRankBy,
  getStudentBy,
  getTeacherBy,
};