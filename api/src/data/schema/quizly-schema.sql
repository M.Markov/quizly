-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema quizly
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema quizly
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `quizly` DEFAULT CHARACTER SET latin1 ;
USE `quizly` ;

-- -----------------------------------------------------
-- Table `quizly`.`roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizly`.`roles` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `quizly`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizly`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `roles_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_users_roles_idx` (`roles_id` ASC) VISIBLE,
  CONSTRAINT `fk_users_roles`
    FOREIGN KEY (`roles_id`)
    REFERENCES `quizly`.`roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `quizly`.`categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizly`.`categories` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(300) NOT NULL,
  `users_id` INT(11) NOT NULL,
  `cover_path` VARCHAR(300) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_categories_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_categories_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `quizly`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `quizly`.`quizzes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizly`.`quizzes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `categories_id` INT(11) NOT NULL,
  `users_id` INT(11) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `duration` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_quizzes_categories1_idx` (`categories_id` ASC) VISIBLE,
  INDEX `fk_quizzes_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_quizzes_categories1`
    FOREIGN KEY (`categories_id`)
    REFERENCES `quizly`.`categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_quizzes_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `quizly`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `quizly`.`types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizly`.`types` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `quizly`.`questions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizly`.`questions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `types_id` INT(11) NOT NULL,
  `quizzes_id` INT(11) NOT NULL,
  `text` VARCHAR(500) NOT NULL,
  `points` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_questions_types1_idx` (`types_id` ASC) VISIBLE,
  INDEX `fk_questions_quizzes1_idx` (`quizzes_id` ASC) VISIBLE,
  CONSTRAINT `fk_questions_quizzes1`
    FOREIGN KEY (`quizzes_id`)
    REFERENCES `quizly`.`quizzes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_questions_types1`
    FOREIGN KEY (`types_id`)
    REFERENCES `quizly`.`types` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `quizly`.`answers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizly`.`answers` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `questions_id` INT(11) NOT NULL,
  `text` VARCHAR(500) NOT NULL,
  `is_correct` TINYINT(4) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_answers_questions1_idx` (`questions_id` ASC) VISIBLE,
  CONSTRAINT `fk_answers_questions1`
    FOREIGN KEY (`questions_id`)
    REFERENCES `quizly`.`questions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `quizly`.`attempts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizly`.`attempts` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `users_id` INT(11) NOT NULL,
  `quizzes_id` INT(11) NOT NULL,
  `points` INT(11) NOT NULL,
  `time` INT(11) NOT NULL,
  `date` DATETIME NULL DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (`id`),
  INDEX `fk_attempts_quizzes1_idx` (`quizzes_id` ASC) VISIBLE,
  INDEX `fk_attempts_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_attempts_quizzes1`
    FOREIGN KEY (`quizzes_id`)
    REFERENCES `quizly`.`quizzes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_attempts_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `quizly`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
