INSERT INTO `roles`(type) VALUES ('teacher'),('student');

-- real password 0000
INSERT INTO users(username, password, first_name, last_name , roles_id)
VALUES ('martin','$2b$10$lJ3ZlosCo3zRCWAMMS0WF.uMESlWZGkJJr61WWWyuC39gTnMqI7Ka','Martin','Markov',(SELECT id FROM roles WHERE type = 'teacher'));

-- real password 123
INSERT INTO users(username, password, first_name, last_name , roles_id)
VALUES ('marinelkata','$2b$10$gOZg4qHLYz1oTYaTbd2jeOUmrLd05B6DmXPpdhmlfX7Jl0eru/GKu','Marinela','Gaydarova',(SELECT id FROM roles WHERE type = 'teacher'));

-- real password 0000
INSERT INTO users(username, password, first_name, last_name , roles_id)
VALUES ('batman','$2b$10$qNuVp1FEc2kEEFnzVW87HO4QafK82YpWhfIx3nt9mYlnSAvmjW9j.','Bruce','Wayne',(SELECT id FROM roles WHERE type = 'student'));

-- real password 0000
INSERT INTO users(username, password, first_name, last_name , roles_id)
VALUES ('robin','$2b$10$L7ZJVEyw4f27MSz8q2OPtOrSSexQ/6AgjvL/uTpEImB0j0CO2S.Ta','Robin','Robin',(SELECT id FROM roles WHERE type = 'student'));

INSERT INTO `types` VALUES (1,'single'),(2,'multiple');

INSERT INTO `categories`(name, description, users_id, cover_path) 
VALUES ('Math','Math quizzes  reviewing basic math concepts.',2,'1604757260863_809348007.jpg'),
('Geography',' Geography quizzes about countries, cities, capitals, bodies of water, mountains ... Basic World Geography Facts.',2,'1604757563112_559094974.jpg'),
('Languages','Language tests can be used to measure the extent to which learners have achieved their goals in a new tongue.',2,'1604757435192_371028667.png'),
('Science','Science quizzes about scientists, body parts, inventors, phobias  etc.',2,'1604757369631_574143269.jpg');

