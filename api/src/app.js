import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import apiVersion from './middlewares/api-version.js';
import { PORT } from './config.js';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';
import createAuthController from './controllers/auth-controller.js';
import createUsersController from './controllers/users-controller.js';
import usersService from './services/users-service.js';
import usersData from './data/users-data.js';
import createCategoriesController from './controllers/categories-controller.js';
import categoriesService from './services/categories-service.js';
import categoriesData from './data/categories-data.js';
import createQuizzesController from './controllers/quizzes-controller.js';
import quizzesData from './data/quizzes-data.js';
import quizzesService from './services/quizzes-service.js';
import handleErrorsMiddleware from './middlewares/handle-errors.js';
import notFoundRouteMiddleware from './middlewares/not-found-route.js';

const app = express();

app.use(cors(), bodyParser.json());
app.use(helmet());
app.use(apiVersion);

app.use(passport.initialize());
passport.use(jwtStrategy);

app.use('/session', createAuthController(usersService, usersData));
app.use('/users', createUsersController(usersService, usersData));
app.use('/categories', createCategoriesController(categoriesService,categoriesData));
app.use('/categories', createQuizzesController(quizzesService, quizzesData));
app.use('/public', express.static('images'));
app.all('*', notFoundRouteMiddleware);

app.use(handleErrorsMiddleware);

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
